const flags = {'remparts1':['kouign-amann_saint-malo_le-corps-de-garde',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
                'remparts2':['robert-surcouf_2-rue-saint-philippe_cimetiere-de-rocabey_l-angleterre_tour-bidouane_22h00',2,1],
                'remparts3':['bastion-de-la-hollande_1534_grand-be',3,1],
                'remparts4':['5.5_5.3',4,1],
				'remparts5':['KL1414_11_13:02_75',5,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_remparts1.addEventListener( "submit", check_and_update);
form_remparts2.addEventListener( "submit", check_and_update);
form_remparts3.addEventListener( "submit", check_and_update);
form_remparts4.addEventListener( "submit", check_and_update);
form_remparts5.addEventListener( "submit", check_and_update);