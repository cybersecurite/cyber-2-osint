---
hide:
  - toc
author: à compléter
title: Remparts
---

# Challenge : remparts

![](../image_chall_breton_intro.png){: .center }

Le célèbre hackeur breton TheKalife vient encore de faire parler de lui en tentant de s'infiltrer dans une base de données d'un organisme sensible de notre pays. Heureusement, il a pu être arrêté à temps !

Guillaume, jeune expert en cybercriminalité qui suit ses activités illicites depuis de nombreux mois, tient enfin une piste qui pourrait permettre de l'appréhender. L'un de ses meilleurs informateurs, un hackeur éthique du nom d'Alexandre, a pu remonter jusqu'à l'une des adresses IP utilisées par TheKalife lors de sa dernière tentative d'intrusion. Malheureusement, celui-ci utilisant de nombreux VPN, Alexandre n'a pu que vaguement le localiser. Toutefois, après avoir réalisé diverses vérifications, il est sûr de l'avoir identifié. Il invite donc Guillaume à le rejoindre.

!!! note "Votre objectif"
    Résoudre les énigmes suivantes pour mettre fin aux agissements de TheKalife.



<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1</h2>
    <p><strong>Mercredi 17 avril 2024, 21h30.</strong><br>Avant de partir rejoindre Alexandre, Guillaume termine son dîner dans une crêperie de la vieille ville avec cette délicieuse pâtisserie emblématique de la région.
    <center><img src='../image_chall_breton_1.png' alt='image' style='width:80%;height:auto;'></center>
    <br>En sortant de la crêperie, il prend depuis les remparts de la ville cette photo souvenir qui, il espère, marquera le début de la fin des agissements de TheKalife.<br><br>
    <center><img src='../image_chall_breton_2.png' alt='image' style='width:80%;height:auto;'></center>
        <ul>
    <li>Quel est le nom de cette pâtisserie ?</li>
    <li>Dans quelle ville se trouve Guillaume ?</li>
    <li>Quel est le nom de la crêperie où Guillaume a dîné ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : palet-breton_pont-aven_la-halte-du-chevalier</span></p>

<p>
    <form id="form_remparts1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2</h2>
<p>Les entrevues entre Guillaume et Alexandre suivent un protocole très particulier : un premier lieu est défini pour fournir les indications nécessaires à l'entrevue qui a lieu obligatoirement le lendemain à 22h précise.
<br><br>
Pour ce premier lieu, Alexandre a fait parvenir à Guillaume cette photo accompagnée du texte suivant : <span style='font-style:italic'>au pied de la tour en face 20240417:2200</span>
<center><img src='../image_chall_breton_3.png' alt='image' style='width:80%;height:auto;'></center>
<ul>
    <li>Qui est ce personnage emblématique de Saint-Malo ?</li>
    <li>Où a-t-il vécu à Saint-Malo durant les 20 dernières années de sa vie ?</li>
    <li>Où est-il enterré ?</li>
    <li>Que montre-t-il du doigt ?</li>
    <li>Quel est le nom de la tour où doit se rendre Guillaume ?</li>
    <li>A quelle heure doit-il être présent à la tour ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : jean-bart_5-rue-sainte-catherine_cimetiere-de-montmartre_l-amerique_tour-solidor_18h15</span></p>

<p>
    <form id="form_remparts2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Enigme 3</h2>
<p>Arrivé sur place à l'heure dite, Guillaume reçoit sur sa messagerie sécurisée le mail suivant d'Alexandre :<br><br>
<span style='font-style:italic;'>Outre plusieurs corsaires et le découvreur du Canada, Saint-Malo eut aussi un célèbre écrivain et diplomate. RDV sépulture.</span><br>
    <ul>
    <li>Où se situe la statue du découvreur du Canada ?</li>
    <li>En quelle année l'a-t-il découvert ?</li>
    <li>Où doivent se retrouver Guillaume et Alexandre le lendemain à 22h ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : bastion-de-la-reine_1615_fort-national</span></p>

<p>
    <form id="form_remparts3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2>Enigme 4</h2>
    <p><strong>Jeudi 18 avril 2024, 21h30.</strong><br>Accoudé aux remparts, Guillaume regarde le chemin vers le Grand Bé que la marée descendante laisse progressivement apparaître.
    <ul>
    <li>En-dessous de quel niveau de la marée en mètre ce chemin est-il praticable en toute sécurité ?</li>
    </ul>
    A 21h50, Guillaume emprunte le chemin vers le Grand-Bé afin de retrouver Alexandre qui lui fournira la dernière information nécessaire à l'arrestation de TheKalife.
    <ul>
    <li>Quelle est le niveau de la marée en mètre au moment où Guillaume emprunte ce chemin ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : 7.1_2.2</span></p>

<p>
    <form id="form_remparts4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Enigme 5</h2>
    <p>La fin de la traque ! Alexandre confie à Guillaume une information cruciale : TheKalife a pris un billet sur le vol Rennes-Amsterdam de 11h35 le samedi 20 avril 2024. 
        <ul>
    <li>Quel est le numéro de ce vol ?</li>
    <li>Avec combien de minutes de retard a-t-il décollé ?</li>
    <li>A quelle heure s'est-il posé à Amsterdam ?</li>
    <li>Combien de temps exprimé en minutes a duré le vol ?</li>
    </ul><span style='font-style:italic;'>Format du flag : AZ2514_25_12:03_88</span></p>

<p>
    <form id="form_remparts5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>A la descente de l'avion, TheKalife est accueilli par des membres d'Interpol. Depuis le hall visiteur, Guillaume et Alexandre savourent le spectacle en pensant que, désormais, le monde est un peu plus sûr...</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques-uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_remparts.js'></script>