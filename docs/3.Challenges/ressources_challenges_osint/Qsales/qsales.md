---
hide:
  - toc
author: à compléter
title: Q salés
---

# Challenge : q salés

![](../image_qsales.png){: .center }

Roxane, conservatrice d’un musée, était troublée. Un tableau représentant une femme qui fut deux fois reine de France avait été volé. La nuit dernière, alors qu’elle achevait les préparatifs pour une exposition spéciale, l’alarme du musée avait retenti, mais il était déjà trop tard. 

!!! note "Votre objectif"
    Retrouver le voleur
	


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>

<h2>Enigme 1</h2>
    <p>Dans les heures qui suivirent, Roxane analysa la scène du crime. Le tableau, un chef-d’œuvre du XVIIIe siècle, semblait avoir disparu sans laisser de traces. 
Aucune caméra n’avait enregistré de mouvement suspect, et les portes étaient restées closes. Seul un message mystérieux laissé sur le bureau, griffonné à la hâte, attira son attention : 
<span style='font-style:italic;'>&laquo; Je suis parti me réfugier loin de toi, que je considère ne pas être légitime pour succéder. &raquo;</span>
<br>
Roxane pressentit que le voleur n'était pas un simple criminel, mais quelqu'un de bien informé. Elle décida de se lancer dans l'enquête. 
<br>
    <ul>
    <li>Quel est le nom de cette Reine ?</li>
    <li>Quel est le nom de la ville où travaille Roxane ?</li>
    <li>Quel est le nom de la ville où doit se rendre Roxane ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : berthe-de-bourgogne_chartres_blois</span></p>


<p>
    <form id="form_Qsales1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>
</div>


<div id="Partie2" hidden>
<br>
<h2>Enigme 2</h2>
<p>
Tout à coup, son téléphone vibra. Un numéro inconnu apparaissait sur l’écran. Elle hésita, mais répondit. Une voix rauque, presque chuchotante, s’éleva.
<span style='font-style:italic;'>&laquo; Roxane, je sais que tu cherches des réponses. Rendez-vous ce soir, à minuit, dans un local à vélo près d'une porte. &raquo;</span>
<br>
Le cœur battant, elle prit note de l’endroit.
<br>
    <ul>
    <li>Quel est le nom de cette porte ?</li>
    <li>Quel est le le numéro du local ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : porte-nom_0123456789</span></p>
	
<p>
    <form id="form_Qsales2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte2'></p>

</div>

<div id="Partie3" hidden>
<br>
<h2>Enigme 3</h2>
<p>Sur place, Roxane trouva dans le local un vélo sur lequel on avait déposé une clé usb. Celle-ci contenait un fichier <a href='../RDV.zip' download='RDV.zip'>RDV.zip</a> contenant des informations. Lentement, l’adrénaline monta en elle. Elle devait se préparer. Armée de courage et de détermination, elle se rendit au rendez-vous. 
<br><br>
Quel est le lieu du rendez-vous ?
</p>

<p>
    <form id="form_Qsales3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte3'></p>
</div>

<div id="Final" markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>
Au fond de la pièce, elle aperçut un homme masqué, sa silhouette familière. C’était l’ombre de son passé, un ancien collègue écarté récemment pour des malversations artistiques. Roxane comprit qu’il était derrière tout cela, motivé par la jalousie.
<br>
Alors qu'il s'apprêtait à quitter le lieu, les sirènes des policiers retentirent dans la nuit...</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_Qsales.js'></script>
