---
hide:
  - toc
author: à compléter
title: Git ou les secrets de la Forge
---

# Challenge : git ou les secrets de la Forge

![](../git.png){: .center }

Il existait il y a encore quelques mois une équipe de cybersécurité.
Malheureusement elle a été fermée car ses membres n'ont pas vraiment respectés les procédures nécessaires...

!!! note "Votre objectif"
    A partir du dernier projet de l'équipe accessible à cette <a href='https://forge.apps.education.fr/cybersecurite/mdps' target='_blank'>adresse</a>, vous devez mener l'enquête pour retrouver diverses informations sur cette dernière.
	
	
??? abstract "Indice"
    L'historique n'est pas disponible, mais la direction a caché bien des choses.
	


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Objectif 1</h2>
Quel est le nom de cette fameuse équipe de cybersécurité ? (quatre lettres en majuscules et avec accent)

<p>
    <form id="form_GitSecrets1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>
</div>


<div id="Partie2" hidden>
<br>
<h2>Objectif 2</h2>
<p>
Maintenant que vous pouvez accéder aux différents fichiers, vous êtes curieux d'en apprendre plus sur leurs pratiques.
<br><br>L'équipe utilisait un mail externe qui a malheureusement fuité. Quelle est cette adresse ?</p>
	

<p>
    <form id="form_GitSecrets2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte2'></p>

</div>

<div id="Partie3" hidden>
<br>
<h2>Objectif 3</h2>
<p>Vous avez trouvé l'email, mais l'historique des mots de passes a été supprimé. 
Retrouvez le mot de passe utilisé par l'équipe ?
</p>


<p>
    <form id="form_GitSecrets3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte3'></p>
</div>

<div id="Final" markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>
Vous obéirez bien aux consignes de sécurité sur un espace public maintenant !
<br><br>
<a href="https://imgflip.com/i/8tat1d"><img src="https://i.imgflip.com/8tat1d.jpg" title="made at imgflip.com"/></a><div><a href="https://imgflip.com/memegenerator">from Imgflip Meme Generator</a></p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>


<script src='../script_chall_GitSecrets.js'></script>
