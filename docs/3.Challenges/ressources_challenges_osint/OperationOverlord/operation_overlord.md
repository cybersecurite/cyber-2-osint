---
hide:
  - toc
author: à compléter
title: Opération Overlord
---

# Challenge : Opération Overlord

![](../image_operation_overlord.png){: .center }
<center><span style='color:gray;font-size:8pt;'>Photo de Robert F. Sargent (domaine public)</span></center>

L'**Opération Overlord** est l'une des grandes batailles de la Seconde Guerre Mondiale sur le théâtre militaire européen.

Mais, la connaissez-vous réellement si bien ? 

!!! note "Votre objectif"
    Résoudre les cinq énigmes


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1</h2>
    <p>Tic tac... tic tac...
       <ul>
           <li>Quel est l'autre nom de cette bataille (3 mots) ?</li>
           <li>Quand s'est-elle déroulée ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : debarquement-de-provence_15-aout-1944_8-mai-1945</span></p>

<p>
    <form id="form_operation_overlord1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2</h2>
<p>La Bataille de Normandie débute avec la plus grande opération amphibie de l'Histoire.
<ul>
    <li>Quel est le nom de code de cette opération ?</li>
    <li>A quelle date a-t-elle eu lieu ?</li>
    <li>En quoi consiste-t-elle (un mot) ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : operation-triton_15-aout-1944_parachutage</span></p>

<p>
    <form id="form_operation_overlord2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Enigme 3</h2>
<p>Le débarquement allié en Normandie a eu lieu simultanément sur cinq plages, dont celle-ci :

<center><img src="../chall_operation_overlord.png" alt="image"></center>

<ul>
    <li>Quel est le nom de cette plage ?</li>
<li>Dans quel département se situe-t-elle ?</li>
<li>Quelles sont les deux villes (par ordre alphabétique) qui en forment les extrémités ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : silver-beach_vendee_bouin_saint-jean-de-monts</span></p>

<p>
    <form id="form_operation_overlord3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2>Enigme 4</h2>
    <p>Le musée du <span style='font-style:italic;'>Débarquement Utah Beach</span> a été créé en 1962 à l'endroit où les troupes américaines ont débarqué le 6 juin 1944.<br><br>
    Quel était le prix d'entrée de ce musée pour une famille de deux adultes, un enfant de 8 ans et un enfant de 5 ans en mai 2020 ?<br><br><span style='font-style:italic;'>Format du flag : 17</span></p>

<p>
    <form id="form_operation_overlord4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>


<div id='Partie5' hidden>
<br>
<h2>Enigme 5</h2>
<p>Le 1 août 1944 débarque à Utah Beach une division blindée française qui participera à la libération de la France. De nos jours, le parcours suivi par cette division est jalonné de bornes leur rendant hommage. L'une d'elle se trouve à Saint-Cyr-l'Ecole.
    <ul>
        <li>Quel est le nom de cette division ?</li>
        <li>Quel est le nom de la place sur laquelle se situe cette borne ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : 1PC_place-de-la-victoire</span></p>

<p>
    <form id="form_operation_overlord5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>L'Opération Overlord n'a pas de secrets pour vous !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_operation_overlord.js'></script>