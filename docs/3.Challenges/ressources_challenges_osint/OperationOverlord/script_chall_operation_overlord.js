const flags = {'operation_overlord1':['bataille-de-normandie_6-juin-1944_25-aout-1944',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
                'operation_overlord2':['operation-neptune_6-juin-1944_debarquement',2,1],
                'operation_overlord3':['utah-beach_manche_quineville_sainte-marie-du-mont',3,1],
                'operation_overlord4':['21',4,1],
				'operation_overlord5':['2DB_place-de-la-republique',5,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_operation_overlord1.addEventListener( "submit", check_and_update);
form_operation_overlord2.addEventListener( "submit", check_and_update);
form_operation_overlord3.addEventListener( "submit", check_and_update);
form_operation_overlord4.addEventListener( "submit", check_and_update);
form_operation_overlord5.addEventListener( "submit", check_and_update);