---
hide:
  - toc
author: à compléter
title: Hiver
---

# Challenge : hiver

![image](https://nuage03.apps.education.fr/index.php/s/6qxmL3Rm5Hcf79L/preview){: .center }

Après leur fameuse randonnée de cet été, Clara et sa jeune soeur Agathe décident de revenir dans la vallée de la Maurienne pour une semaine de vacances à la neige.

!!! note "Votre objectif"
    Résoudre les 11 énigmes

!!! warning "Avertissement"
    Ce challenge étant long comme une nuit d'hiver, n'essayez pas de le faire en une seule fois. Faites des pauses ! Par conséquent, n'oubliez pas de garder une trace écrite des différents flags que vous avez trouvés.


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1</h2>
    <p><strong>Jour 1 : samedi 22 février 2025.</strong><br>Arrivées à la gare de Modane, les deux soeurs décident de faire un petit tour en ville en attendant leur bus qui les amènera à leur station. Juste après avoir franchi la D1006 en sortant de la gare, elles font cette photo depuis une petite place en retrait sur la gauche.
        <center><img src='https://nuage03.apps.education.fr/index.php/s/FrM6GaiLipiLsEX/preview' alt=''></center>
        <br>
        <ul>
    <li>Quel est le nom du fort ?</li>
    <li>Quels sont les numéros des deux parcelles cadastrales formant le côté ouest de la place où elles se trouvent ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : fort-des-tetes_1492-1515</span></p>

<p>
    <form id="form_hiver1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2</h2>
<p>Une fois arrivé au charmant petit village de Lanslevillard où elles ont leur location, elles prennent cette photo.
    <center><img src='https://nuage03.apps.education.fr/index.php/s/5Xi2p9ReeMzTYec/preview' alt=''></center>
    <br>
<ul>
    <li>Quel est le nom de la station où elles se trouvent ?</li>
    <li>A combien culmine cette montagne ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : la-plagne_4807</span></p>

<p>
    <form id="form_hiver2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Enigme 3</h2>
<p><strong>Jour 2 : dimanche 23 février 2025.</strong><br>Avant de partir en randonnée en raquette, les deux soeurs vont chercher un peu de ravitaillement dans le village voisin de Lanslebourg. Dans la rue principale, elles prennent en photo le monument d'un animal emblématique de la vallée.
    <center><img src='https://nuage03.apps.education.fr/index.php/s/TJ7EPD6y2E4nCHA/preview' alt=''></center>
    <br>
    Elles prennent ensuite la rue à gauche portant le même nom que le télésiège qu'elles vont emprunter dans quelques instants.<br><br>
    <ul>
    <li>Quel est le nom de cet animal ?</li>
        <li>Quel était son métier ?</li>
    <li>A quel endroit Clara et Agathe vont-elles faire une randonnée ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : lanterne_eclaireur_col-de-la-madeleine</span></p>

<p>
    <form id="form_hiver3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2>Enigme 4</h2>
    <p>Arrivées à destination de leur randonnée, elles prennent cette photo.<br>
    <center><img src='https://nuage03.apps.education.fr/index.php/s/R534XwfZazPB8bt/preview' alt=''></center>
        <br>
    <ul>
        <li>Quel est le nom du fort que l'on voit sur cette photo ?</li></ul>
    Outre ce fort, le col du Mont-Cenis regorge de nombreuses autres fortifications utilisées jusqu'à la fin de la Seconde Guerre Mondiale.<br><br>
    <ul>
    <li>Combien y a-t-il de fortifications réparties sur le col ?</li>
    <li>Parmi toutes ces fortifications, combien y en avait-il de françaises ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : rocher-de-l-aigle_9_louis-ferdinand_francois-thibault-de-menonville</span></p>

<p>
    <form id="form_hiver4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Enigme 5</h2>
    <p><strong>Jour 3 : lundi 24 février 2025.</strong><br>Le lendemain matin, Clara et Agathe décident de partir faire une  randonnée en ski de fond sur le domaine nordique de Bessans. Quelques heures et 250 mètres de dénivelé positif plus tard, elles s'offrent un chocolat chaud et une crêpe à la confiture de myrtille, puis terminent leur journée en escaladant une cascade de glace.
        <ul>
    <li>Quel est le nom de la piste qu'elles ont empruntée ?</li>
    <li>Combien de kilomètres ont-elles parcourues ?</li>
            <li>Où se situe la cascade de glace ?</li>
    </ul><span style='font-style:italic;'>Format du flag : triathlon_18.3_rocher-du-chateau</span></p>

<p>
    <form id="form_hiver5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>

<div id='Partie6' hidden>
<br>
<h2>Enigme 6</h2>
    <p><strong>Jour 4 : mardi 25 février 2025.</strong><br>Après une bonne nuit de repos, Clara et Agathe partent pour une randonnée en raquette pour admirer et escalader une aiguille incontournable de la vallée. En chemin, elles prennent cette photo.
        <center><img src='https://nuage03.apps.education.fr/index.php/s/iS4A8WRLAM2sxYL/preview' alt=''></center>
        <br>
        <ul>
    <li>Quel est le nom de cette aiguille ?</li>
    <li>A quelle hauteur en mètres culmine-t-elle ?</li>
            </ul>
    Arrivées sur place, elles décident de passer par la voie ouverte sur la face sud-est de l'aiguille.<br><br>
    <ul>
    <li>Quel est le nom de cette voie ?</li>
        <li>Quand a-t-elle été ouverte ?</li>
        <li>Quelles sont les cotations minimale et maximale de cette voie ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : menhir-de-carnac_78_reine-des-neiges_hiver-1899_3a-4b</span></p>

<p>
    <form id="form_hiver6">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte6'></p>
</div>


<div id='Partie7' hidden>
<br>
<h2>Enigme 7</h2>
    <p>Un peu plus haut au dessus du monolithe se situe un outil de communication datant du XIXème siècle.
    <ul>
    <li>Quel est cet outil ?</li>
        <li>A quelle ligne de communication appartient-il ?</li>
        <li>Quel est le numéro de ce poste de communication ?</li>
    </ul><span style='font-style:italic;'>Format du flag : telephone-portable_paris-marseille_12</span></p>

<p>
    <form id="form_hiver7">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte7'></p>
</div>


<div id='Partie8' hidden>
<br>
<h2>Enigme 8</h2>
    <p><strong>Jour 5 : mercredi 26 février 2025.</strong><br>Aujourd'hui, Clara et Agathe décident d'admirer le panorama depuis la Canopée des Cîmes, puis de redescendre manger au restaurant d'altitude accessible depuis le télécabine du Vieux Moulin.
<ul>
    <li>A quelle altitude se situe cette Canopée ?</li>
    <li>Comment y accède-t-elle ?</li>
    </ul>
    Pour redescendre vers le restaurant, elles empruntent tout d'abord une piste portant le nom d'une skieuse célèbre, puis enchaîne sur une piste dont le nom est assez explicite sur le niveau de difficulté.
    <br><br>
<ul>
    <li>Quelles sont les couleurs de ces deux pistes ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : 1500_telecabine-du-vieux-moulin_verte-bleue</span></p>

<p>
    <form id="form_hiver8">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte8'></p>
</div>



<div id='Partie9' hidden>
<br>
<h2>Enigme 9</h2>
    <p><strong>Jour 6 : jeudi 27 février 2025.</strong><br>
        Aujourd'hui, direction le charmant village d'Aussois à partir duquel elles souhaitent faire une petite excursion jusqu'à la barrière de l'Esseillon.
        <ul>
    <li>Combien de fortifications comporte l'Esseillon ?</li>
    </ul>
    Elles commencent par faire une petite pause au fort Marie-Christine où elles admirent le panorama.<br><br>
    <ul>
    <li>A combien culmine le sommet situé à 140&deg; Est ?</li>
    </ul>
    Elles poursuivent ensuite la visite des différents forts pour redescendre jusqu'à la redoute Marie-Thérèse. En chemin, elles prennent cette photo en se remémorant les différents parcours de via ferrata qu'elles ont effectués l'été dernier.<br><br>
    <center><img src='https://nuage03.apps.education.fr/index.php/s/aEE7WjyQmMRrKmZ/preview' alt=''></center>
    <br>
    <ul>
        <li>Combien de parcours arrivent aux batteries basses de ce fort ?</li>
        <li>Combien de parcours partent de ce même point ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : 3_2500_5_2</span></p>

<p>
    <form id="form_hiver9">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte9'></p>
</div>



<div id='Partie10' hidden>
<br>
<h2>Enigme 10</h2>
    <p><strong>Jour 7 : vendredi 28 février 2025.</strong><br>
        Pour leur dernière journée, Clara et Agathe décident d'aller visiter ce charmant petit village
        <center><img src='https://nuage03.apps.education.fr/index.php/s/JL7nZi65NAZ8Aax/preview' alt''></center>
        <br>
        Celui-ci a été l'un des lieux utilisé dans un film datant de 2013<br><br>
        <ul>
    <li>Quel est l'identifiant de la notice de ce film ?</li>
    <li>Sur quelle chaîne et à quelle date a-t-il été diffusé pour la première fois à la télévision ?</li>
    <li>Quels ont été les heures de début et de fin de diffusion à la seconde près ?</li>
    </ul><span style='font-style:italic;'>Format du flag : 0123456.789_TF1-15/11/2013_15:52:03-18:03:46</span></p>

<p>
    <form id="form_hiver10">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte10'></p>
</div>



<div id='Partie11' hidden>
<br>
<h2>Enigme 11</h2>
    <p><strong>Jour 8 : samedi 1 mars 2025.</strong><br>
        C'est le jour du départ ! Clara et Agathe prennent le bus à 10h26 à l'arrêt situé devant la télécabine du Vieux Moulin pour se rendre à la gare routière de Saint-Michel-de-Maurienne.
    <ul>
    <li>A quelle heure le bus les dépose-t-il à la gare routière ?</li>
    <li>Sur la photo que l'on peut trouver sur Google Street View, combien de bus se trouvait dans cette gare en juin 2008 ?</li>
    </ul><span style='font-style:italic;'>Format du flag : 10:32_5</span></p>

<p>
    <form id="form_randonnee11">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte11'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous êtes incollable sur la vallée de la Maurienne !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_hiver.js'></script>