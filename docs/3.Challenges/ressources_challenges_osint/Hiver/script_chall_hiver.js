const flags = {'hiver1':['fort-du-replaton_0763-0764',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
                'hiver2':['val-cenis_3695',2,1],
                'hiver3':['flambeau_vaguemestre_col-du-mont-cenis',3,1],
                'hiver4':['fort-de-variselle_4_1',4,1],
				'hiver5':['marathon_23.3_rocher-des-barmettes',5,1],
				'hiver6':['monolithe-de-sardieres_93_dieu-des-elfes_automne-1989_5b-6c',6,1],
				'hiver7':['telegraphe-chappe_lyon-milan_22',7,1],
				'hiver8':['2800_telesiege-de-la-met_noire-rouge',8,1],
				'hiver9':['5_3104_1_4',9,1],
				'hiver10':['5636665.001_M6-21/12/2015_21:02:34-22:48:29',10,1],
				'hiver11':['11:45_2',11,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_hiver1.addEventListener( "submit", check_and_update);
form_hiver2.addEventListener( "submit", check_and_update);
form_hiver3.addEventListener( "submit", check_and_update);
form_hiver4.addEventListener( "submit", check_and_update);
form_hiver5.addEventListener( "submit", check_and_update);
form_hiver6.addEventListener( "submit", check_and_update);
form_hiver7.addEventListener( "submit", check_and_update);
form_hiver8.addEventListener( "submit", check_and_update);
form_hiver9.addEventListener( "submit", check_and_update);
form_hiver10.addEventListener( "submit", check_and_update);
form_hiver11.addEventListener( "submit", check_and_update);