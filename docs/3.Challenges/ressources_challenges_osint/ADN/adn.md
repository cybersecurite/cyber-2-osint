---
hide:
  - toc
author: à compléter
title: Un brin peut en cacher un autre
---


# Challenge : un brin peut en cacher un autre

![](../adn.png){: .center }

Au cœur d'une forêt dense et mystérieuse, un groupe de botanistes se réunit autour d'une plante énigmatique. Ses feuilles, d'un vert profond et lustré, semblaient presque translucides sous la lumière de la lune. Personne n'avait jamais vu une telle merveille auparavant. Sur ses pétales, des motifs étranges apparaissaient, rappelant des symboles anciens.

!!! note "Votre objectif"
    Résoudre les quatre énigmes


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Qui suis-je ?</h2>
<p>
Le chef du groupe, le Dr Kalifir, tenait dans ses mains une note trouvée près de la plante. <span style='font-style:italic;'>&laquo; Cette plante cache un secret &raquo;</span>, lisait-il à haute voix. <span style='font-style:italic;'>&laquo; Pour le découvrir, vous devrez résoudre une énigme : mon jour est le 29 germinal et je suis commune. Qui suis-je ? &raquo;</span>
       <ul>
           <li>A quelle date cela correspond-il ?</li>
           <li>Quel est mon nom scientifique (deux mots) ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : 28-juin_passiflora-caerulea</span></p>

<p>
    <form id="form_adn1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Séquençage</h2>
<p>Le lendemain matin, l’équipe se mit à analyser la plante sous toutes ses coutures. Le Dr Kalifounet, spécialiste en génétique végétale, proposa de séquencer l'ADN du chloroplaste de la plante. 
<br>&laquo; Peut-être que son secret se cache dans son code génétique &raquo;, suggéra-t-elle.
<br>Les autres acquiescèrent. Ils prélevèrent un échantillon de la feuille et commencèrent le processus de séquençage. Puis ils analysèrent les résultats dans une banque de séquences nucléiques
<ul>
    <li>Quel est le rôle du chloroplaste (un mot) ?</li>
    <li>Combien de gènes possède-t-il ?</li>
    <li>Quels sont le symbole et l'identifiant du gène de la protéine de la membrane de l'enveloppe du chloroplaste ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : reparation_258_abcD_12345678</span></p>

<p>
    <form id="form_adn2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Nucléotides</h2>
<p>
Pendant des heures, ils scrutèrent les nucléotides : adénine (A), thymine (T), cytosine (C) et guanine (G). Chaque lettre semblait porter une partie du mystère. Ils notèrent qu'une certaine séquence revenait fréquemment : ATCG. 
<br>Cette répétition avait certainement une signification. <br>
Ces questions étaient cruciales pour comprendre la structure et le fonctionnement de l'ADN de la plante.
<ul>
    <li>Combien de nucléotides possède le génome de mon chloroplaste ?</li>
<li>Quelle est la séquence de nucléotides située entre le numéro 50051 et 50060 ?</li>
<li>Combien de nucléotides possède le gène ndhI ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : 123456_**********_275</span></p>

<p>
    <form id="form_adn3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2>Mutation ?</h2>
    <p>Après de longues heures de travail acharné, ils réussirent enfin à déchiffrer une partie du message.<br>
    &laquo; Regardez ça ! &raquo; s'exclama Dr Kalifounet. &laquo; Nous avons quelque chose ! &raquo;
    <br>Cependant, alors qu'ils continuaient à explorer le fichier du génome, ils trouvèrent un autre document numérique caché dans les données séquencées. Ce <a href='../genome_chloroplaste_myrtille.txt' download='genome_chloroplaste_myrtille.txt'>fichier</a> contenait une série de chiffres apparemment aléatoires.
<br>
    &laquo; Qu'est-ce que c'est ? &raquo; demanda le petit Marius, l'assistant du Dr Kalifir.
<br>
    Le Dr Kalifounet, intrigué, se pencha sur l'écran. 
<br>&laquo; C'est un autre code. Peut-être un message crypté. &raquo;
 <br>Ils transférèrent le fichier sur leur ordinateur principal et commencèrent à analyser les données. 
 <br>Le fichier contenait un message mystérieux. Armés de ce nouvel indice, ils se préparèrent à explorer le nouveau lieu.
<br><br>
    Quel est son numéro SIRET ?<br><br><span style='font-style:italic;'>Format du flag : 81340514111125</span></p>

<p>
    <form id="form_adn4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Ainsi, l’énigme de la plante mystérieuse marqua le début d'une aventure scientifique sans précédent, où chaque découverte ouvrait la porte à de nouvelles questions et à de nouveaux mystères à résoudre. Le génome de la myrtille n'a plus de secrets pour vous !</p>
<br>
<p> Mais cette aventure n'est pas terminée et continue dans ce nouveau lieu. A tout de suite sur le challenge <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/CadenasAlea/cadenas/" target="_blank">Cadenas dynamiques</a></p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_adn.js'></script>