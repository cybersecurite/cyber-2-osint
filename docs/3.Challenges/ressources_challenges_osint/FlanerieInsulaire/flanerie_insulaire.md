---
hide:
  - toc
author: à compléter
title: Flânerie insulaire
---

# Challenge : flânerie insulaire

![](../image_flanerie_insulaire.png){: .center }

En vacance en Charente-Maritime, je décide d'aller visiter aujourd'hui un charmant village situé sur l'une des nombreuses îles du littoral atlantique.

Une fois passé le deuxième plus long pont de France, j'aperçois sur le côté gauche les restes de l'ancien embarcadère qui permettait de rejoindre l'île à l'aide d'un bac.

 - En quelle année le bac a-t-il cessé de fonctionner ?

Arrivé à destination, je me dirige vers la place du village et je suis étonné de voir que le clocher de son église est peint en noir et blanc.

 - Quel est le nom de ce village ?
 - A quoi servait autrefois ces peintures (nom technique) ?

Une fois la visite du village effectuée, je décide de déjeuner sur le port à côté de l'ancienne gare de chemin de fer.

 - Sur quelle période (années de début et de fin) a roulé ce train ?
 - Quel était le surnom de ce train ?

En repartant du village en début d'après-midi, je me dirige vers la &laquo; capitale &raquo; de l'île afin de découvrir ses riches ouvrages fortifiés inscrits au patrimoine mondial de l'Humanité par l'Unesco.

 - Qui a réalisé ces fortifications ?
 - A quelle date furent-elles inscrites au patrimoine mondial de l'Humanité par l'Unesco ?

Au sortir du village pour rentrer sur le continent, j'aperçois un animal couvert de longs poils et emblématique de la région.

 - Quelle est la race de cet animal ?

!!! note "Votre objectif"
    Déterminez le flag

    Format du flag : <span style='font-style:italic;'>2012_beaulieu-sur-dordogne_repere_1914-1918_le-mairat_mansart_9-octobre-2020_tigre-du-bengale</span>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">


<p>
    <form id="form_flanerie_insulaire">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Votre flânerie sur l'île de Ré s'est parfaitement déroulée !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_flanerie_insulaire.js'></script>