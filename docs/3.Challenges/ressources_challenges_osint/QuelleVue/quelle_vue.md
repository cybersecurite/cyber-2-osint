---
hide:
  - toc
author: à compléter
title: Quelle vue !
---

# Challenge : quelle vue !

![](../image_chall_quelle_vue_intro.png){: .center }

Enfin ! Dernière volée de marches et photo magnifique !

![](../image_chall_quelle_vue.png){: .center }

 - Combien de marches ai-je montées ?
 - Quel est le nom du phare que l’on voit au large ?

Pour arriver jusqu’ici du continent, j’ai traversé un pont.

 - Quelle est sa longueur, exprimée en mètres ?
 - Quand a-t-il été mis en service ?

!!! note "Votre objectif"
    Déterminez le flag

    Format du flag : <span style='font-style:italic;'>148_phare-de-calais_1435.8_25-juin-1982</span>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">


<p>
    <form id="form_quelle_vue">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez traversé l'Ile de Ré de bout en bout !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_quelle_vue.js'></script>