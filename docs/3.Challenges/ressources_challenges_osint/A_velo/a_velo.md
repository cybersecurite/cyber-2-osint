---
hide:
  - toc
author: à compléter
title: A vélo
---

# Challenge : à vélo !

![](../image_intro_a_velo.png){: .center }

Cet été, petite balade en vélo d'une semaine en famille sur l'une des plus remarquables pistes cyclables de la région. En chemin, j'ai pris cette magnifique photo.

![](../image_chall_a_velo.png){: .center }

 - Quel est le numéro de cette écluse ?
 - Quel est le numéro de la parcelle sur laquelle le bâtiment est construit ?
 - Quel est le nom de cette piste cyclable ?
 - Quelles sont les deux villes qui en forment les extrémités (par ordre alphabétique) ?

!!! note "Votre objectif"
    Déterminez le flag

    Format du flag : <span style='font-style:italic;'>17_AB0123_la-chartreuse_dijon-paris</span>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">


<p>
    <form id="form_a_velo">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>La coulée verte le long du canal de Nantes à Brest n'a pas de secret pour vous !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_a_velo.js'></script>