---
hide:
  - toc
author: à compléter
title: Trésor disparu
---


# Challenge : trésor disparu

![](../tresor_perdu.png){: .center }

Un trésor disparu depuis des siècles est sur le point de resurgir...

Jeune étudiant en histoire et féru d'architecture, tu te trouves à un carrefour crucial de ta jeune carrière. Entre les murs anciens de la bibliothèque de ton université, tu plonges dans des manuscrits poussiéreux et des archives oubliées, guidé par une passion insatiable pour les mystères du passé. Depuis des mois, tu es obsédé par une énigme vieille de plusieurs siècles, une énigme qui a défié les plus grands esprits de l'Histoire.

Aujourd'hui, une lueur d'espoir scintille à l'horizon. 

La route est encore longue et semée d'embûches, mais tu es prêt à affronter chaque défi avec détermination et ingéniosité. Ta quête n'est pas seulement pour la gloire ou le trésor ; elle est motivée par le désir de dévoiler une partie oubliée de notre héritage commun, de redonner vie à une histoire enfouie sous les décombres du temps. L'aventure ne fait que commencer, et chaque jour de recherche te rapproche un peu plus de la résolution de cette énigme fascinante.

!!! note "Votre objectif"
    Résoudre les différents énigmes


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Aux temps anciens</h2>
<h3>Un incroyable trésor</h3>
    <p>Après la déroute des armées de César devant Vercingétorix en 52 avant J.-C., un butin considérable fut amené à Nemossos, la capitale des Arvernes. D'après l'historien grec Strabon, cette ville est située sur les bords d'un fleuve.<br><br>Quel est le nom ancestral de ce fleuve ?<br><br>
    <span style='font-style:italic;'>Format du flag : sequana</span></p>

<p>
    <form id="form_tresor_perdu1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2></h2>
<h3>Transfert</h3>
<p>Lors du déclin de la ville, le trésor fut transféré dans la ville d'Augustonemetum.<br><br>Cette ville changea de nom plusieurs fois au cours des siècles.
<ul>
    <li>Quel est son nom au IVème siècle ?</li>
    <li>Quel est son nom actuel ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : cenabum_orleans</span></p>

<p>
    <form id="form_tresor_perdu2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2></h2>
<h3>Un nouveau gardien</h3>
<p>Pendant plusieurs siècles, ce trésor resta entre les murs de cette ville, tout d'abord dans la cathédrale, puis dans la basilique fondée par l'évêque Saint Avit.<br><br>Quel est le nom actuel de cette basilique ?
<br><br>
    <span style='font-style:italic;'>Format du flag : saint-jean-de-latran</span></p>

<p>
    <form id="form_tresor_perdu3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2></h2>
<h3>Disparition...</h3>
    <p>A la veille du pillage de la ville par Pépin le Bref, le trésor fut à nouveau déplacé et tomba peu à peu dans l'oubli...<br><br>
    En quelle année cela se passe-t-il ?<br><br><span style='font-style:italic;'>Format du flag : 476</span></p>

<p>
    <form id="form_tresor_perdu4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>De nos jours</h2>
<h3>Une découverte étonnante</h3>
    <p>Vos recherches vous conduisent à la Bibliothèque du Patrimoine de Clermont Auvergne Métropole au département de l'Auvergne pittoresque de 1780 à 1860. Vous y découvrez un vieux manuscrit en latin très abîmé accompagné d'un fragment de morceau d'estampe :
        <center><img src='../image_chall_chateau.png' alt='image'></center>
        Au verso, vous découvrez également le texte suivant qui semble être une traduction parcellaire du manuscrit :<br><br>
        <span style='font-style:italic;'>[...]<br>gardien solitaire sur son éperon rocheux<br>domine la vallée de [...]<br>qui, telle un serpent de pluie ondule [...]<br>Depuis l'arche l'enjambant vers [...]<br>St. M. protège [...]<br>
        trésor arv[...]<br>
        en son lieu le plus sacré</span>
        <br><br>
 Assurément, quelqu'un est déjà venu faire des recherches sur le fameux trésor arverne disparu depuis des siècles, car ce document ne devrait pas être rangé ici...<br><br>
        Dans quel lieu (village et monument) se trouve le trésor ?<br><br><span style='font-style:italic;'>Format du flag : paris_mairie</span></p>

<p>
    <form id="form_tresor_perdu5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>

<div id='Partie6' hidden>
<br>
<h2></h2>
<h3>Encore, et toujours, lui !</h3>
    <p>Avec une équipe de votre département, vous vous précipitez à cette abbaye. Après de nombreuses recherches utilisant les technologies les plus élaborées, vous découvrez un escalier secret caché sous l'autel de l'église. Fébrilement, vous le descendez et vous longez un couloir sombre et humide pour atteindre une porte ferrée en chêne massif dont la serrure semble avoir été forcée...<br><br>
        Vous ouvrez la porte et vous tombez sur... RIEN !!!<br><br>
        Enfin... presque rien...<br><br>
        Sur le sol, deux petites pièces anciennes posées l'une sur l'autre et, à côté, une petite plaque gravée :
        <center><img src='../image_chall_gravure.png' alt='gravure'></center><br><br>
    Qui est passé avant vous et où se trouve le trésor ?<br><br><span style='font-style:italic;'>Format du flag : sherlock-holmes_chateau-gaillard</span></p>

<p>
    <form id="form_tresor_perdu6">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte6'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez résolu l'énigme du trésor arverne !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_tresor_perdu.js'></script>