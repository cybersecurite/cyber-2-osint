const flags = {'tresor_perdu1':['liger',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
                'tresor_perdu2':['arvernis_clermont-ferrand',2,1],
                'tresor_perdu3':['notre-dame-du-port',3,1],
                'tresor_perdu4':['761',4,1],
				'tresor_perdu5':['menat_abbaye',5,1],
				'tresor_perdu6':['arsene-lupin_aiguille-creuse',6,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_tresor_perdu1.addEventListener( "submit", check_and_update);
form_tresor_perdu2.addEventListener( "submit", check_and_update);
form_tresor_perdu3.addEventListener( "submit", check_and_update);
form_tresor_perdu4.addEventListener( "submit", check_and_update);
form_tresor_perdu5.addEventListener( "submit", check_and_update);
form_tresor_perdu6.addEventListener( "submit", check_and_update);