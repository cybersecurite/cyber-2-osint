---
hide:
  - toc
author: à compléter
title: Espionnage
---

# Challenge : espionnage

![](../image_SH.png){: .center }


<h2>Partie 1 : le rendez-vous</h2>
<p>Vous devez rencontrer un de nos agents infiltré chez l'ennemi afin qu'il puisse vous donner des informations capitales sur un projet en cours. Suivant le canal habituel, il nous a envoyé le lieu du rendez-vous qui doit se situer dans un parc que seuls les initiés peuvent identifier.</p>

!!! note "Votre objectif"
    Déterminez le lieu du rendez-vous.

    Le flag est le nom de la ville et le nom du parc écrits dans leur langue d'origine, en minuscules, sans accents, sans caractères spéciaux et sans espaces.
    
    Par exemple, s'il s'agit du parc André-Citroën situé à Paris, on écrira `paris_parc_andre-citroen`.


??? abstract "Indice"

    <center><img src = '../indice0.jpg' style='width:55%;height:auto;'></center>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">


<p>
    <form id="form_EspionnagePartie1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>



<div id='Final' hidden>
<br>
<p><a href='../espionnage2'>Poursuivez votre mission...</a></p>
</div>

<script src='../script_chall_espionnage.js'></script>