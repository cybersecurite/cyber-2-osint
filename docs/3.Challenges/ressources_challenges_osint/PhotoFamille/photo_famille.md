---
hide:
  - toc
author: à compléter
title: Photo de famille
---

# Challenge : photo de famille

![](../intro_photo_famille.png){: .center }

Une photo... Un groupe scientifique parmi les plus importants du XXème siècle...

Arriverez-vous à faire le lien entre les deux ?

!!! note "Votre objectif"
    Résoudre les deux énigmes proposées


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1</h2>
    <p>Au cours d'une randonnée, j'ai pris cette photo d'un ensemble de bâtiments liés à l'Université Clermont Auvergne.<br><br> Ces bâtiments ont vu, il y a presque 100 ans, le congrès fondateur d'un groupe scientifique très important.<br>
    <center><img src='../image_challenge.png' alt='photo challenge'></center>
    <br>
<ul>
           <li>En quelle année a eu lieu ce congrès ?</li>
           <li>Quel était le nom de ce groupe ?</li>
           <li>Quels étaient les noms (par ordre alphabétique) des membres fondateurs de ce groupe présents sur la photo prise à cette époque ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : 1870_general_cauchy-fourier-galois-laplace-malgrange-ramis</span></p>

<p>
    <form id="form_photo_famille1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2</h2>
<p>L'oeuvre du groupe Bourbaki, débutée en 1939, est un traité de Mathématiques composé de onze livres répartis en différents fascicules.
<ul>
    <li>Quel est le numéro du livre contenant le fascicule XII ?</li>
    <li>Quelle est l'année de la première édition de ce fascicule ?</li>
    <li>De combien de chapitres ce fascicule est-il constitué ?</li>
    <li>Quel est le numéro du dernier chapitre de ce fascicule et quel est le troisième mot de ce chapitre ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : VII_1941_9_12-fonction</span></p>

<p>
    <form id="form_photo_famille2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Le groupe Bourbaki n'a pas de secrets pour vous !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_photo_famille.js'></script>