const flags = {'randonnee1':['vallee-de-la-maurienne_l-arc',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
                'randonnee2':['hannibal_napoleon-I_felix-faure',2,1],
                'randonnee3':['via-alpina_chemin-du-petit-bonheur_col-de-l-iseran',3,1],
                'randonnee4':['barriere-de-l-esseillon_5_victor-emmanuel_marc-rene-de-montalembert',4,1],
				'randonnee5':['via-ferrata-du-diable_la-montee-au-ciel_la-descente-aux-enfers-la-montee-au-purgatoire',5,1],
				'randonnee6':['les-14-chapeaux_termignon_bleu-de-termignon',6,1],
				'randonnee7':['diable-de-bessans',7,1],
				'randonnee8':['refuge-d-averole_l-albaron_3637',8,1],
				'randonnee9':['faudan_chapelle-sainte-marguerite',9,1],
				'randonnee10':['S53_10:30_cite-des-ducs_fontaine_elephants',10,1],
				'randonnee11':['lac-du-bourget_lamartine_abbaye-d-hautecombe',11,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_randonnee1.addEventListener( "submit", check_and_update);
form_randonnee2.addEventListener( "submit", check_and_update);
form_randonnee3.addEventListener( "submit", check_and_update);
form_randonnee4.addEventListener( "submit", check_and_update);
form_randonnee5.addEventListener( "submit", check_and_update);
form_randonnee6.addEventListener( "submit", check_and_update);
form_randonnee7.addEventListener( "submit", check_and_update);
form_randonnee8.addEventListener( "submit", check_and_update);
form_randonnee9.addEventListener( "submit", check_and_update);
form_randonnee10.addEventListener( "submit", check_and_update);
form_randonnee11.addEventListener( "submit", check_and_update);