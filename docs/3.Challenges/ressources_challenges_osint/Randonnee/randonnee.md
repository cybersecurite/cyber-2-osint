---
hide:
  - toc
author: à compléter
title: Randonnée
---

# Challenge : randonnée

![](../image_randonnee.png){: .center }

Clara, jeune étudiante brillante en cybersécurité, vient de découvrir le sujet de NSI que sa non moins brillante soeur cadette Agathe a eu lors de son épreuve de bac Métropole, juin 2024, jour 2. Quelle n’a pas été sa surprise de découvrir des noms de lieux qu’elle connait bien !!!

Leur année scolaire brillamment terminée, les deux soeurs décident donc de partir quelques semaines en randonnée dans les Alpes pour revoir des lieux qui ont nourri leur enfance.

!!! note "Votre objectif"
    Résoudre les onze énigmes

!!! warning "Avertissement"
    Ce challenge étant long comme une randonnée de dix jours, n'essayez pas de le faire en une seule fois. Faites des pauses ! Par conséquent, n'oubliez pas de garder une trace écrite des différents flags que vous avez trouvés.


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1</h2>
    <p><strong>Jour 1 : samedi 27 juillet 2024.</strong><br>Après quelques heures de train, Clara et Agathe arrivent enfin à la gare de Modane, point de départ de leur périple dans la vallée. A peine sorties, elles entendent déjà le doux murmure du torrent qui le traverse dans toute sa longueur et au bord duquel elles ont souvent joué.
        <ul>
    <li>Quel est le nom de cette vallée ?</li>
    <li>Quel est le nom du torrent ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : vallee-de-la-tarentaise_l-eure</span></p>

<p>
    <form id="form_randonnee1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2</h2>
<p>La vallée de la Maurienne a connu de nombreux personnages historiques, parmi lesquels un général carthaginois cherchant à franchir les Alpes, un empereur qui fit aménager la route du Mont Cenis que l'on emprunte toujours actuellement, et un président de la IIIème République lors de son déplacement en Savoie suite à son rattachement à la France. Un refuge à son nom a d'ailleurs été construit au début du XXème siècle pour commémorer cet événement.
<br><br>Qui sont ces trois personnages historiques ?
<br><br>
    <span style='font-style:italic;'>Format du flag : hamilcar_leopold-II_raymond-poincare</span></p>

<p>
    <form id="form_randonnee2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Enigme 3</h2>
<p>Les chaussures lacées, les sacs remplis sur le dos, les bâtons saisis et les cartes topographiques IGN à portée de main (les téléphones passent très mal en montagne et les applications telles que Google Map ne sont pas assez précises...), les deux soeurs commencent leur périple en suivant pendant 9 jours l’intégralité du GR5E qui les mènera jusqu’au dernier village qu’il dessert en amont dans la vallée.
    <ul>
    <li>Ce GR prend deux noms différents au cours de la traversée de la vallée. Quels sont ces deux noms ?</li>
    <li>Le dernier village auquel elles doivent se rendre se trouve au pied d’un célèbre col gravi de nombreuses fois par les coureurs du Tour de France. Quel est le nom de ce col ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : via-appia_route-du-grand-bornand_col-de-l-izoard</span></p>

<p>
    <form id="form_randonnee3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2>Enigme 4</h2>
    <p>Parties de la gare de Modane, Clara et Agathe atteignent enfin après quelques heures de marche le but de leur première étape : un site exceptionnel de fortifications du XIXème siècle construit par le royaume de Piémont-Sardaigne selon la technique de fortifications perpendiculaires due à un ingénieur français du XVIIIème siècle.<ul>
    <li>Quel est le nom de ce site ?</li>
    <li>Combien de forts y a-t-il ?</li>
    <li>Quel est le nom du fort principal ?</li>
    <li>Quels sont le prénom et le nom de famille de l'ingénieur français ?</li>
    </ul><span style='font-style:italic;'>Format du flag : rocher-de-l-aigle_9_louis-ferdinand_francois-thibault-de-menonville</span></p>

<p>
    <form id="form_randonnee4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Enigme 5</h2>
    <p><strong>Jour 2 : dimanche 28 juillet 2024.</strong><br>Le lendemain matin, Clara et Agathe décident de faire une partie de la via ferrata installée sur la Barrière de l’Esseillon. Sportives de haut niveau et alpinistes chevronnées, elles partent du Pont du Diable pour rejoindre le fort Victor-Emmanuel, puis redescendre à la Cascade du Nant.
        <ul>
    <li>Quel est le nom de cette via ferrata ?</li>
    <li>Quels sont les noms <strong>complets</strong> des deux tronçons qu’elles empruntent ?</li>
    </ul><span style='font-style:italic;'>Format du flag : via-ferrata-des-enfers_le-parcours-des-angelots_la-descente-aux-limbes-la-voie-des-mages</span></p>

<p>
    <form id="form_randonnee5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>

<div id='Partie6' hidden>
<br>
<h2>Enigme 6</h2>
    <p><strong>Jour 3 : lundi 29 juillet 2024.</strong><br>Après une bonne nuit de repos, Clara et Agathe reprennent la route du GR5E. Prochaine étape : le troisième village de la station de Val-Cenis. Peu avant Bramans, elles croisent le repère d’une célèbre bande de bandits de grands chemins qui terrorisa la population au début du XIXème siècle.
        <ul>
    <li>Quel est le nom de cette bande ?</li>
    <li>Quel est le nom du village où Clara et Agathe terminent leur étape du jour ?</li>
    <li>Ce village est le seul village au monde où l'on fabrique un fromage d'exception. Quel est le nom de ce fromage ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : les-4-daltons_beaufort_brie-de-coulommiers</span></p>

<p>
    <form id="form_randonnee6">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte6'></p>
</div>


<div id='Partie7' hidden>
<br>
<h2>Enigme 7</h2>
    <p><strong>Jour 4 : mardi 30 juillet 2024.</strong><br>Cette étape, relativement facile, amène les deux jeunes soeurs au petit village de Bessans où un personnage particulier les attend sur la place du village.<br><br>
    Qui est ce personnage ?<br><br><span style='font-style:italic;'>Format du flag : marmotte-de-termignon</span></p>

<p>
    <form id="form_randonnee7">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte7'></p>
</div>


<div id='Partie8' hidden>
<br>
<h2>Enigme 8</h2>
    <p><strong>Jours 5-6-7 : mercredi 31 juillet 2024 au vendredi 2 août 2024.</strong><br>Depuis Bessans, plusieurs vallons s’ouvrent sur la droite. Clara et Agathe s’engagent dans l’un d’eux vers un refuge dans lequel elles ont réservé pour deux nuits. En remontant le vallon, elles prennent le refuge en photo, puis, arrivées sur place, elles prennent en photo leur défi du lendemain : l'un des sommets majeurs de la vallée de la Maurienne.
<center><img src='../image_chall_randonnee.png' alt='montagne'></center>
<center><img src='../image_chall_randonnee_bis.png' alt='montagne'></center>
<br>
        Parties vers 2h du matin, elle suivent la voie normale permettant d’atteindre le fameux sommet par sa face sud. De retour au refuge en début d’après-midi, elles profitent de la journée du vendredi pour prendre un peu de repos.
<ul>
    <li>Quel est le nom du refuge ?</li>
    <li>Quel est le nom du sommet qu'ont escaladé Clara et Agathe ?</li>
            <li>A combien culmine-t-il ?</li>
    </ul>
    <span style='font-style:italic;'>Indice : il existe des outils spécialisés pour repérer des sommets à partir d'un lieu donné</span>
    <br><br>
    <span style='font-style:italic;'>Format du flag : refuge-d-ambin_l-arcelle_3312</span></p>

<p>
    <form id="form_randonnee8">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte8'></p>
</div>



<div id='Partie9' hidden>
<br>
<h2>Enigme 9</h2>
    <p><strong>Jour 8 : samedi 3 août 2024.</strong><br>
        Après cette bonne journée de repos amplement méritée, Clara et Agathe quittent le refuge pour rejoindre le GR5E et Bonneval-sur-Arc, dernière étape de leur randonnée en Maurienne. Arrivées peu avant midi, elles décident de poursuivre leur chemin jusqu'au petit hameau de l'Ecot afin d'y déjeuner en profitant du panorama depuis sa petite chapelle. En chemin, selon la légende, elle passe sur un ancien village englouti par un éboulement de la montagne.
        <ul>
    <li>Quel est le nom du village englouti ?</li>
    <li>Quel est le nom de la chapelle où Clara et Agathe vont déjeuner ?</li>
    </ul><span style='font-style:italic;'>Format du flag : tignes_chapelle-sainte-anne</span></p>

<p>
    <form id="form_randonnee9">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte9'></p>
</div>



<div id='Partie10' hidden>
<br>
<h2>Enigme 10</h2>
    <p><strong>Jour 9 : dimanche 4 août 2024.</strong><br>
        Après avoir consulté les horaires sur le site officiel de Bonneval-sur-Arc, Clara et Agathe montent dans le bus à la patinoire du village à destination de la gare routière de Saint-Michel de Maurienne afin d'y prendre le train de 13h57 à destination de Chambéry.
        <ul>
    <li>Quel est le numéro de la ligne empruntée par Clara et Agathe ?</li>
    <li>A quelle heure sont-elles parties de Bonneval-sur-Arc ?</li>
    </ul>
        A l'arrivée à Chambéry, elles décident d'aller voir l'un des plus célèbres monuments de la ville : <span style='font-style:italic;'>les quatre sans cul</span><ul>
    <li>Comment est surnommée la ville de Chambéry ?</li>
    <li>Quel est le monument que Clara et Agathe vont voir à Chambéry ?</li>
    <li>Qui sont les <span style='font-style:italic;'>quatre sans cul</span> ?</li>
    </ul><span style='font-style:italic;'>Format du flag : T2_14:37_cite-des-papes_chateau_marmottes</span></p>

<p>
    <form id="form_randonnee10">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte10'></p>
</div>



<div id='Partie11' hidden>
<br>
<h2>Enigme 11</h2>
    <p><strong>Jour 10 : lundi 5 août 2024.</strong><br>
        Pour leur dernier jour en Savoie, Clara et Agathe décident d'aller se promener sur les bords d'un célèbre lac situé à une douzaine de kilomètres de Chambéry et au bord duquel se situe la nécropole de la Maison de Savoie.<ul>
    <li>Comment s'appelle ce lac ?</li>
    <li>Ce lac a inspiré l'un des plus célèbres poèmes de la poésie romantique. Qui en est l'auteur ?</li>
    <li>Dans quel lieu précis se situe la nécropole de la Maison de Savoie ?</li>
    </ul><span style='font-style:italic;'>Format du flag : lac-de-geneve_vigny_chapelle-d-aigueblanche</span></p>

<p>
    <form id="form_randonnee11">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte11'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous êtes incollable sur la vallée de la Maurienne !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_randonnee.js'></script>