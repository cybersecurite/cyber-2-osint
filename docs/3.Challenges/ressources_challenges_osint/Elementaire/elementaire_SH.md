---
hide:
  - toc
author: à compléter
title: Elémentaire... Vous avez dit élémentaire ?
---

# Challenge : Elémentaire... Vous avez dit élémentaire ?

![](../image_SH_nuit.png){: .center }

Suivez les pas d'un des plus célèbres détectives de l'Histoire.

Mais, connaissez-vous réellement tout sur lui ?

<center><span style='font-style:italic;'>&laquo; Lorsque vous avez éliminé l'impossible, ce qui reste,<br>si improbable soit-il, est nécessairement la vérité. &raquo;</span></center>

!!! note "Votre objectif"
    Résoudre les six énigmes proposées


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1</h2>
    <p>Même les plus grands ont <span style='font-style:italic;'>une ennemie</span><br><br><span style='font-style:italic;'>Format du flag : ines-dupont</span></p>

<p>
    <form id="form_elementaire_SH1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2</h2>
<p>Cette histoire aurait pu s'appeler <span style='font-style:italic;'>Le chat d'Aubergenville</span>, mais... il n'en a rien été
<ul>
    <li>Quel est le titre original de cette histoire ?</li>
    <li>En quelle année a-t-elle commencée à être publiée ?</li>
    <li>De combien de chapitres est-elle composée ?</li>
    <li>Combien de fois le nom de notre détective apparaît-il parmi les 400 pages de l'édition de 1902 de cette histoire ?</li>
    </ul>
<span style='font-style:italic;'>Format du flag : the-cat-of-aubergenville_1826_53_145</span></p>

<p>
    <form id="form_elementaire_SH2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Enigme 3</h2>
<p>Un ennemi juré… Un ultime combat… Tout problème a une solution…
<ul>
    <li>Quel est le titre original de la nouvelle mettant en scène ce combat ?</li>
<li>Quelle est la date exacte de ce combat ?</li>
<li>Où ce combat a-t-il eu lieu ?</li>
    </ul>
    <span style='font-style:italic;'>Format du flag : a-famous-problem_6-juin-1944_falaises-d-etretat</span></p>

<p>
    <form id="form_elementaire_SH3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2>Enigme 4</h2>
<p>Notre célèbre détective a été porté à l'écran plus de 250 fois, que ce soit dans des films ou des séries télévisées.<br><br>En 1985, un film met en scène une version jeune de notre détective. Dans ce film, celui-ci doit résoudre une énigme de haute intensité qui viendra le marquer à jamais et affronter un terrible ennemi.<br><br>
    Quels sont les trois noms (par ordre d'apparition dans le film) pris par cet ennemi redoutable ?<br><br><span style='font-style:italic;'>Format du flag : dupont_durand_duchmol</span></p>

<p>
    <form id="form_elementaire_SH4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>


<div id='Partie5' hidden>
<br>
<h2>Enigme 5</h2>
<p>A partir de 2010, une adaptation moderne des aventures de notre détective a vu le jour avec Benedict Cumberbatch dans le rôle titre.<br><br>Sur quelle chaîne, quel jour et à quelle heure (à la seconde près) a été diffusé le premier épisode de cette série en France ?<br><br><span style='font-style:italic;'>Format du flag : france5_27-05-2018_14:05:48</span></p>

<p>
    <form id="form_elementaire_SH5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>


<div id='Partie6' hidden>
<br>
<h2>Enigme 6</h2>
<p>Une copie transparente de notre détective est mise en scène par un écrivain français afin de l'opposer à son héros au cours de deux aventures mémorables.<br><br>Quels sont, dans l'ordre de leur publication, les titres de ces deux nouvelles ?<br><br><span style='font-style:italic;'>Format du flag : le-manteau-rouge_le-livre-grec</span></p>

<p>
    <form id="form_elementaire_SH6">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte6'></p>
</div>





<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Le grand Sherlock Holmes n'a pas de secrets pour vous !</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_elementaire_SH.js'></script>