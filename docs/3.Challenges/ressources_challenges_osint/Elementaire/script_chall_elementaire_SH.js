const flags = {'elementaire_SH1':['irene-adler',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
                'elementaire_SH2':['the-hound-of-the-baskervilles_1901_15_200',2,1],
                'elementaire_SH3':['the-final-problem_4-mai-1891_chutes-du-reichenbach',3,1],
                'elementaire_SH4':['rathe_ihtar_moriarty',4,1],
				'elementaire_SH5':['france4_01-01-2011_20:35:28',5,1],
                'elementaire_SH6':['la-dame-blonde_la-lampe-juive',6,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_elementaire_SH1.addEventListener( "submit", check_and_update);
form_elementaire_SH2.addEventListener( "submit", check_and_update);
form_elementaire_SH3.addEventListener( "submit", check_and_update);
form_elementaire_SH4.addEventListener( "submit", check_and_update);
form_elementaire_SH5.addEventListener( "submit", check_and_update);
form_elementaire_SH6.addEventListener( "submit", check_and_update);