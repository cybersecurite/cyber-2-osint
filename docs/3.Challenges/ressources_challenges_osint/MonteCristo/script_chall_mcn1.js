const flags = {'mcn11':['chateau_if_43.2_5.3',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
               'mcn12':['X_1',2,1],
               'mcn13':['17_novembre_1755_19',3,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
    {
	texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
	{
	    document.getElementById('Final').hidden = false;
	}
	else
	{
	    const val_part = flags[f_name][1]+flags[f_name][2];
	    document.getElementById('Partie'+val_part.toString()).hidden = false;
	}
    }
    else 
    {
	texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
    }
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_mcn11.addEventListener( "submit", check_and_update);
form_mcn12.addEventListener( "submit", check_and_update);
form_mcn13.addEventListener( "submit", check_and_update);
