---
hide:
  - toc
author: à compléter
title: Montecristo I
---


# Challenge : Montecristo I

![](../Le_comte_de_Monte-Cristo_-_Jules_Rouff.png){: .center }
<center><span style='color:gray;font-size:8pt;'>Auteur anonyme, domaine public, via Wikimedia Commons</span></center>

A l'origine un *roman feuilleton*, puis deux volumes intégraux, puis des films.

Nous sommes en 2024. Il est donc temps, 180 ans plus tard, de se plonger dans les mystères d'**Edmond Dantès**. 

!!! note "Votre objectif"
    Résoudre les trois énigmes pour découvrir le trésor du comte.

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Où suis-je ?</h2>
<p> Pour débuter notre voyage, nous devons trouver les indices laissés par le mentor de Dantès, <em>l'abbé Faria</em>.
</p>

<p>Voici une partie du premier lieu:
<ol>
	<li>comment s'appelle-t-il ?</li>
	<li>Où est-ce <em>précisément</em> selon OpenStreetMap ? (latitude et longitude)</li>
</ol>

<center>
<img src="../enigme1.jpg" alt="Un mur en en pierre dans un cachot qui est devenu une attraction touristique">
</center>

<span style='font-style:italic;'>Format du flag : maison_chene_54.3_6.4</span>
</p>

<form id="form_mcn11">
	<label for="flag">Flag : </label>
	<input type="text" name="flag" style='background-color:black;color:white;' size=50>
	<input type="submit" value="Vérifier">
</form>

<p id='Texte1'></p>

</div>


<div id='Partie2' hidden>
<br>
<h2>Un message caché</h2>
<p>Après avoir attendu votre tour pour entrer dans cette cellule hautement touristique, vous avez une vision d'ensemble sur les murs. Deux messages particuliers vous sautent aux yeux.</p>

<pre>LE P_TI_ CAB_N__ D_S _U___R___</pre>

<pre>Timidité, me voici là bien défini
Discrétion, elle est ma principale qualité
Si vous m'avez trouvé, au lointain je m'enfuis
Pas plus d'une occurence de moi vous ne verrez
</pre>


<p>Quel est le numéro du chapitre et du volume d'où est extrait ce texte ?<br><br><span style='font-style:italic;'>Format du flag : XIX_7</span></p>


<form id="form_mcn12">
	<label for="flag">Flag : </label>
	<input type="text" name="flag" style='background-color:black;color:white;' size=50>
	<input type="submit" value="Vérifier">
</form>

<p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Un roi en disgrâce</h2>
<p>Vous avez percé le secret du mur et celui-ci s'ouvre sous vos yeux fatigués par la poussière.
Derrière, un simple coffret et un <a href="https://fr.wikisource.org/wiki/Le_Comte_de_Monte-Cristo/Chapitre_10" target='_blank'>feuillet</a> sur lequel figure l'inscription <em>le comte de monte-cristo, Chapitre X</em> vous attendent.
</p>


<p>Sur le coffret, est traçée la phrase suivante dans la poussière : <em>La naissance du Roi et toutes ses mentions</em>.

<p>Quel est le mot de passe ?<br><br><span style='font-style:italic;'>Format du flag : 18_janvier_1254_72</span></p>
<p>
    <form id="form_mcn13">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez poursuivi une partie de l'aventure d'Edmond à travers le web et les mots.
Si vous souhaitez relever le défi, un futur challenge plus corsé vous attendra bientôt.</p>

!!! danger "A retenir"

    L'OSINT est de nos jours une branche incontournable de la cybersécurité et est utilisée principalement dans le cadre d'activités liées à la sécurité nationale, la vérification d'informations et la lutte contre les _fake news_.

    De nombreux outils de recherche (voir par exemple cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/' target='_blank'>page</a> qui en liste quelques uns) permettent de pratiquer cette activité.

    Attention toutefois au fait qu'il faut rester dans un cadre légal ! En France, il est interdit d’accéder à des informations privées sans permission et d’utiliser les données pour nuire.

</div>

<script src='../script_chall_mcn1.js'></script>
