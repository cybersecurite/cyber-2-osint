---
hide:
  - nav
  - toc
author: à compléter
title: Liste des challenges
---

# OSINT


!!! abstract "Description générale de la catégorie"

    L'OSINT (en anglais : open source intelligence ; en français : renseignement d'origine sources ouvertes) est un ensemble de pratiques d’investigation et d’analyse qui vise par "ingénierie sociale" à dévoiler une information préalablement dissimulée en récoltant, croisant ou analysant des données numériques disponibles en source ouverte (médias, web, données gouvernementales, publications professionnelles et académiques, données commerciales, etc.)

    L'OSINT est principalement utilisé dans le cadre d'activités liées à la sécurité nationale, l'application de la loi et l'intelligence économique dans le secteur privé. La pratique est également particulièrement utilisée par les journalistes et les citoyens engagés dans la lutte contre les _fake news_.

    Attention, tout n'est pas permis :
    le fait de télécharger un fichier clairement privé de cette manière pourrait être interprété par la justice comme une action illégale.
    Enfin, rappelons que le doxing, qui consiste à diffuser publiquement des informations personnelles (même si ces informations ont été obtenues légalement) est puni par la loi (article 223-1-1 du code pénal).

    Dans cette catégorie, on propose divers challenges d'entraînement à ces différentes pratiques (légales) de l'OSINT.


!!! warning "Autres challenges"

    D'autres challenges dans différentes catégories sont également disponibles sur le <a href='https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/' target='_blank'>site principal</a> dédié à la cybersécurité.

    Etes-vous prêt à relever les défis ?


<hr style="height:5px;color:red;background-color:red;">

!!! note "A vélo"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/A_velo/image_intro_a_velo.png){: .center }
    
    Une petite semaine de balade à vélo en famille...

    Ce qu'une photo sur les réseaux sociaux peut amener...

    [_Accéder au challenge_](../ressources_challenges_osint/A_velo/a_velo){ .md-button rel="noopener" }

!!! note "Elémentaire... Vous avez dit élémentaire ?"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 6 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/Elementaire/image_SH_nuit.png){: .center }
    
    Suivez les pas d'un des plus célèbres détectives de l'Histoire.
    
    Une enquête de haut vol où vous devrez faire preuve de toute votre sagacité pour résoudre les différentes énigmes proposées.

    [_Accéder au challenge_](../ressources_challenges_osint/Elementaire/elementaire_SH){ .md-button rel="noopener" }

!!! note "Espionnage (d'après Passe ton hack d'abord)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 2 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/Espionnage/image_SH.png){: .center }
    
    En tant qu'agent secret, vous devez exécuter une mission secrète à haut risque. Votre contact vous a donné rendez-vous dans un lieu secret... L'affaire ne fait que commencer...

    [_Accéder au challenge_](../ressources_challenges_osint/Espionnage/espionnage){ .md-button rel="noopener" }

!!! note "Flânerie insulaire"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/FlanerieInsulaire/image_flanerie_insulaire.png){: .center }
    
    Des vacances... une île à découvrir... partons à la découverte d'un riche patrimoine...

    [_Accéder au challenge_](../ressources_challenges_osint/FlanerieInsulaire/flanerie_insulaire){ .md-button rel="noopener" }

!!! note "Git ou les secrets de la Forge"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 3 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/GitSecrets/git.png){: .center }

    Une équipe de cybersécurité qui n'a pas respecté les règles a été supprimée... Retrouverez-vous leurs traces ?

    [_Accéder au challenge_](../ressources_challenges_osint/GitSecrets/gitsecrets){ .md-button rel="noopener" }


!!! note "Hiver"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐⭐ 11 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](https://nuage03.apps.education.fr/index.php/s/6qxmL3Rm5Hcf79L/preview){: .center }
    
    La suite du challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Randonnee/randonnee/'>Randonnée</a> dans lequel Clara et Agathe proposaient une  splendide randonnée dépaysante dans les Alpes... Elles reviennent ici sur les lieux de leurs derniers exploits, mais dans un décor de neige et de glace ! Arriverez-vous à résoudre toutes les énigmes ?

    [_Accéder au challenge_](../ressources_challenges_osint/Hiver/hiver){ .md-button rel="noopener" }


!!! note "Montecristo I"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 3 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/MonteCristo/Le_comte_de_Monte-Cristo_-_Jules_Rouff.png){: .center }
    <center><span style='color:gray;font-size:8pt;'>Auteur anonyme, domaine public, via Wikimedia Commons</span></center>

    Le comte a bien des secrets. A vous de les percer pour naviguer dans ce roman épique.

    [_Accéder au challenge_](../ressources_challenges_osint/MonteCristo/mcn1){ .md-button rel="noopener" }


!!! note "Opération Overlord"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 5 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/OperationOverlord/image_operation_overlord.png){: .center }
    <center><span style='color:gray;font-size:8pt;'>Photo de Robert F. Sargent (domaine public)</span></center>
    
    Plongez dans la plus grande bataille de la Seconde Guerre Mondiale et faites preuve de toute votre perspicacité pour résoudre les différentes énigmes qui vont sont proposées...

    [_Accéder au challenge_](../ressources_challenges_osint/OperationOverlord/operation_overlord){ .md-button rel="noopener" }


!!! note "Photo de famille"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 2 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/PhotoFamille/intro_photo_famille.png){: .center }
    
    Une photo... Un groupe scientifique parmi les plus importants du XXème siècle...

    Arriverez-vous à faire le lien entre les deux ?

    [_Accéder au challenge_](../ressources_challenges_osint/PhotoFamille/photo_famille){ .md-button rel="noopener" }


!!! note "Q Salés"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 3 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/Qsales/image_qsales.png){: .center }
    
    Roxane, conservatrice d’un musée, était troublée. Un tableau représentant une femme qui fut deux fois reine de France avait été volé. La nuit dernière, alors qu’elle achevait les préparatifs pour une exposition spéciale, l’alarme du musée avait retenti, mais il était déjà trop tard...

    [_Accéder au challenge_](../ressources_challenges_osint/Qsales/qsales){ .md-button rel="noopener" }

!!! note "Quelle vue !"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/QuelleVue/image_chall_quelle_vue_intro.png){: .center }
    
    Que découvrir à partir d'une simple photo anodine ?

    [_Accéder au challenge_](../ressources_challenges_osint/QuelleVue/quelle_vue){ .md-button rel="noopener" }


!!! note "Randonnée"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐⭐ 11 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/Randonnee/image_randonnee.png){: .center }
    
    Suivez les pas de Clara et Agathe dans une splendide randonnée dépaysante dans les Alpes... Arriverez-vous à résoudre toutes les énigmes ?

    [_Accéder au challenge_](../ressources_challenges_osint/Randonnee/randonnee){ .md-button rel="noopener" }


!!! note "Remparts"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 5 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/Remparts/image_chall_breton_intro.png){: .center }
    
    Le hackeur breton TheKalife a encore frappé. Aidez Guillaume et Alexandre à mettre fin à ses agissements...

    [_Accéder au challenge_](../ressources_challenges_osint/Remparts/remparts){ .md-button rel="noopener" }



!!! note "Trésor disparu"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐⭐ 6 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/TresorDisparu/tresor_perdu.png){: .center }

    Un trésor disparu depuis des siècles est sur le point de resurgir...
    
    Jeune étudiant en histoire et féru d'architecture, vous êtes sur le point de résoudre l'une des plus grandes énigmes de notre temps...

    [_Accéder au challenge_](../ressources_challenges_osint/TresorDisparu/tresor_disparu){ .md-button rel="noopener" }


!!! note "Un brin peut en cacher un autre"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐⭐ 4 flags

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/ADN/adn.png){: .center }

    Plongez au coeur d'un génome mystérieux et explorez-le dans ses moindres recoins...

    Un brin d'ADN peut cacher bien des secrets...

    [_Accéder au challenge_](../ressources_challenges_osint/ADN/adn){ .md-button rel="noopener" }


!!! note "(veni, vidi, vici)x6"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** manipuler divers outils d'OSINT

    ![](../ressources_challenges_osint/VeniVidiVicix6/image_cesar.png){: .center }
    
    La Guerre des Gaules fait rage... César et Vercingétorix ont à nouveau levé des armées pour se combattre. Qui l'emportera ?

    [_Accéder au challenge_](../ressources_challenges_osint/VeniVidiVicix6/venividivicix6){ .md-button rel="noopener" }


